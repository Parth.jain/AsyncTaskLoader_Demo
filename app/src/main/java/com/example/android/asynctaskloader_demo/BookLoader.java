package com.example.android.asynctaskloader_demo;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;

/**
 * Created by parth on 15/3/18.
 */

class BookLoader extends AsyncTaskLoader<String> {
    private String mQueryString;
    public BookLoader(Context context, String queryString) {
        super(context);
        mQueryString=queryString;

    }

    @Override
    public String loadInBackground() {
        return NetworkUtils.getBookInfo(mQueryString);
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }
}
